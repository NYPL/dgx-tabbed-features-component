## Changelog

### v0.2.0
- Upgrading to React 15.

### v0.1.6
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.
- Added `gaActionText` property to generate a proper Google Analytics action text.

#### Changed
- Updated README.md file to contain a CHANGELOG and enhanced component description.

### v0.1.7
#### Added
- Added support for Google Analytics tracking for the **tab heading titles**.

#### Changed
- Updated the Google Analytics **action text** for each element to include the **tab heading titles**.
