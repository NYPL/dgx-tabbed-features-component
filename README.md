# Tabbed Features Component

This component is used to display the DOM elements with tabs in the dgx-homepage app.

## Version
> v0.2.0

## Usage

> Require `dgx-tabbed-features-component` as a dependency in your `package.json` file.

```sh
"dgx-tabbed-features-component": "git+ssh://git@bitbucket.org/NYPL/dgx-tabbed-features-component#master"
```

> Once installed, import the component in your React Application.

```sh
import TabbedComponent from 'dgx-tabbed-features-component';
```

## Props

> You may initialize the component with the following properties:

```sh
<TabbedComponent
  id="tabbedComponent" ## String (default: 'tabbedComponent')
  className="tabbedComponent" ## String (default: 'tabbedComponent')
  lang="en" ## String representing the language (default: 'en')
  items={[]} ## Object containing tab elements
  index={1} ## Number representing the index of the tab (optional, currently not used)
  action={func()} ## Function sent to TitleTabs for onClick handler
  error="Error Message" ## String representing the error message of no items are found
  gaClickEvent={gaClickFunc()} ## Function for Google Analytics click events. The Action is determined by the gaActionText property and concatenated with the item number. The Label is the URL for each feature element (Optional)
  gaActionText="What's Happening" ## String representing the base of what the Action field will be on each feature item (Optional)
/>
```

## Local Development Setup
Run `npm install` to install all dependencies from your package.json file.

Next, startup the Webpack Development Server by running `npm start`. Visit `localhost:3000` in your web browser and begin coding.

> **NOTE:** We are currently using Webpack Hot Reload Server configuration which allows you to change your code without restarting your browser.

Once you have completed any code updates, ensure to run `npm run build` in your terminal. This will build/bundle all your code in the `dist/` path via Webpack.
