import React from 'react';
import { render } from 'react-dom';
import TabbedComponent from './components/TabbedComponent/TabbedComponent.js';

/* app.jsx
 * Used for local development of React Components
 */

// This mock data has the same structure as the data we get from the Refinery
// http://dev-refinery.nypl.org/api/nypl/ndo/v0.1/site-data/containers?
// filter%5Bname%5D=What%27s%20Happening&include=children
const dummyContent = {
  type: 'container',
  id: '6f236d1d-a53b-4b25-9fd9-27bd7249b928',
  name: {
    en: {
      text: 'What\'s Happening',
    },
    es: {
      text: 'Que Está Pasando',
    },
  },
  children: [
    {
      type: 'container',
      id: '288cf593-8af0-4f8f-aaec-10fc0eb2e1ae',
      name: {
        en: {
          text: 'Exhibitions',
        },
      },
      slots: [
        {
          title: {
            en: {
              text: '100 Years of the Picture Collection: From Abacus to Zoology',
            },
          },
          category: '',
          description: {
            en: {
              text: 'The New York Public Library is celebrating the centennial of its Picture ' +
                'Collection with a new exhibition in the Wachenheim Gallery at the Stephen A. ' +
                'Schwarzman Building.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%2012.43.31%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/exhibitions/picturecollection100',
          date: {
            en: {
              text: 'Happening Now',
            },
          },
        },
        {},
        {
          title: {
            en: {
              text: 'Magical Designs for Mozart\'s Magic Flute',
            },
          },
          category: '',
          description: {
            en: {
              text: 'This exhibition compares scenic and costume designs from a select group ' +
                'of 20th and 21st century productions.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%201.14.50%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/exhibitions/magical-designs-mozarts-magic-flute',
          date: {
            en: {
              text: 'Coming Soon',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Shakespeare\'s Star Turn in America',
            },
          },
          category: '',
          description: {
            en: {
              text: 'The exhibition focuses on Shakespeare\'s plays in North America from the ' +
              'colonial times to the present.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%201.19.38%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/exhibitions/shakespeares-star-turn-america',
          date: {
            en: {
              text: 'Coming Soon',
            },
          },
        },
      ],
    },
    {
      type: 'container',
      id: '2db0d1a1-0b90-4e8d-8afd-5346472e051a',
      name: {
        en: {
          text: 'Readings & Conversations',
        },
      },
      slots: [
        {
          title: {
            en: {
              text: 'Adult Coloring Book Club',
            },
          },
          category: '',
          description: {
            en: {
              text: 'Join the Kips Bay Library for its first Adult Coloring Book Group! ',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%201.27.41%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/01/05/adult-coloring-book-club',
          date: {
            en: {
              text: 'Tues, Jan 26 | 1 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Sunday Movie: The Thin Man',
            },
          },
          category: '',
          description: {
            en: {
              text: 'Nick and Nora Charles, a former detective and his playful wife, ' +
                'investigate a murder case mostly for the fun of it.',
            },
          },
          image: {
            rectangularImage: {
              'full-url': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%201.39.26%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/07/sunday-movie-thin-man',
          date: {
            en: {
              text: 'Coming Soon',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Gotham Jazzmen',
            },
          },
          category: '',
          description: {
            en: {
              text: 'A concert of traditional jazz presented by the Gotham Jazzmen. ',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%201.47.37%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/03/29/gotham-jazzmen',
          date: {
            en: {
              text: 'Coming Soon',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Beethoven\'s FIDELIO recital by New York Opera Forum',
            },
          },
          category: '',
          description: {
            en: {
              text: 'New York Opera Forum will perform  the complete opera of FIDELIO by ' +
                'Ludwig van Beethoven.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/Screen%20Shot%202016-01-07%20at%201.55.15%20PM.png',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/08/13/music-beethovens-fidelio-performed-new-york-opera-' +
            'forum',
          date: {
            en: {
              text: 'Coming Soon',
            },
          },
        },
      ],
    },
    {
      type: 'container',
      id: 'ecb7bc7c-ae63-45c6-80da-f94304b5149b',
      name: {
        en: {
          text: 'Performances & Films',
        },
      },
      slots: [
        {
          title: {
            en: {
              text: 'Talks at the Schomburg: Basquiat and Contemporary Queer Art',
            },
          },
          category: '',
          description: {
            en: {
              text: 'The Very Black Project, Dr. Jordana Saggese, Kim Drew and Dr. David ' +
                'Clinton Wills will present a #veryqueer conversation on the life and legacy' +
                ' of Jean Michel-Basquiat.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/basquiat.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/01/talks-schomburg-basquiat-and-contemporary-queer-art',
          date: {
            en: {
              text: 'Mon, Feb 1 | 6:30 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Stage for Debate: Respectability and Activism',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: 'For our inaugural debate at the Schomburg Center, we will discuss ' +
              'respectability and its relationship to activism and black liberation movements ' +
              'in the U.S. and abroad.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/stage%20for%20debate%20collage%201.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/02/stage-debate-respectability-and-activism',
          date: {
            en: {
              text: 'Tues, Feb 2 | 6:30 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Black Deutschland: Darryl Pinckney and Zadie Smith ',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: 'Darryl Pinckney and Zadie Smith talk about his new work of fiction, ' +
                'Black Deutschland. Click on the event page to reserve your free seats!',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/zadie3.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/10/black-deutschland-darryl-pinckney-and-zadie-smith',
          date: {
            en: {
              text: 'Tues, Feb 9 | 7 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Sudden Death: Álvaro Enrigue and Rivka Galchen',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: 'Álvaro Enrigue and Rivka Galchen talk about his new novel, Sudden Death. ' +
                'Reserve your free seats!',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/talk.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/26/sudden-death-alvaro-enrigue-and-rivka-galchen',
          date: {
            en: {
              text: 'Thurs, Feb 25 | 7 PM',
            },
          },
        },
      ],
    },
    {
      id: '21a46849-f0be-4e2e-8491-e10f542b70eb',
      name: {
        en: {
          text: 'Gatherings & Activities',
        },
      },
      slots: [
        {
          title: {
            en: {
              text: 'Between the Lines: Eddie Glaude Jr. and Imani Perry',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: ' Democracy In Black is a landmark book on race in America, and author' +
                'Eddie Glaude Jr. will be in conversation with Dr. Imani Perry, professor at ' +
                'Princeton University.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/talk2.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/03/between-lines-eddie-glaude-jr-and-imani-perry',
          date: {
            en: {
              text: 'Wed, Feb 3 | 6:30 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'New York and the American Folk Music Revival, with Stephen Petrus',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: 'This lecture explores New York\'s central role in fueling the nationwide' +
                'amaze for folk music in postwar America—one of the great cultural phenomena of' +
                'the 20th century.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/moon.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/09/folk-city-new-york-and-american-folk-music-revival' +
            '-stephen-petrus',
          date: {
            en: {
              text: 'Tues, Feb 9 | 6:30 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Word Nerd: Dispatches from the Games, Grammar and Geek Underground',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: 'This illustrated lecture explores anagrams, palindromes, the birth of ' +
                'the World SCRABBLE Championship, and many of the more colorful figures who ' +
                'inhabit this subculture.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/scrabble.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/02/11/word-nerd-dispatches-games-grammar-and-geek-' +
            'underground-john-d-williams',
          date: {
            en: {
              text: 'Thurs, Feb 11 | 6:30 PM',
            },
          },
        },
        {
          title: {
            en: {
              text: 'Street Smart: The Rise of Cities and Fall of Cars, with Samuel I. Schwartz',
            },
          },
          category: {
            en: {
              text: 'Event',
            },
          },
          description: {
            en: {
              text: 'This illustrated lecture presents the millennial revolution of the urban' +
                'landscape and the rise of the pedestrian, the cyclist, and the public ' +
                'transportation commuter.',
            },
          },
          image: {
            rectangularImage: {
              'full-uri': 'https://d7.nypl.org/sites/default/files/car.jpg',
              description: 'this is the image description',
              alt: 'this is the image description',
            },
          },
          link: 'http://www.nypl.org/events/programs/2016/03/01/street-smart-rise-cities-and-fall-cars-' +
            'samuel-i-schwartz',
          date: {
            en: {
              text: 'Tues, March 1 | 6:30 PM',
            },
          },
        },
      ],
    },
  ],
};

// Used to mock gaClick event
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

render(
  <TabbedComponent
    items={dummyContent.children}
    gaClickEvent={gaClickTest()}
    gaActionText="What's Happening"
  />,
  document.getElementById('tabbed')
);
