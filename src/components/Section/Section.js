// Import React
import React from 'react';
import PropTypes from 'prop-types';
import {
  map as _map,
  isEmpty as _isEmpty,
} from 'underscore';
// Import components
import EventItem from './../EventItem/EventItem.js';

// For a slot that contains four items
const SectionItem = (props) => {
  /**
   * getText(eventObj)
   * @desc Extact the value of text from event object and return it.
   * @param (Object) eventObj
   * @return String
   */
  const getText = (eventObj = {}) => (
    (eventObj[props.lang] && eventObj[props.lang].text) ?
      eventObj[props.lang].text : ''
  );

  /**
   * renderEvents(events)
   * @desc Render EventItem Components.
   * @param (Array) events
   * @return Object
   */
  const renderEvents = (events) => (
    _map(events, (event, i) => {
      if (_isEmpty(event)) {
        return undefined;
      }

      const link = event.link || '';
      const location = event.location || '';
      const imageContent = (event.image && event.image.rectangularImage) ?
        event.image.rectangularImage : {};
      const {
        'full-uri': imageSrc = '',
        alt: imageAlt = '',
      } = imageContent;

      return (
        <EventItem
          key={i}
          num={i}
          headingTitle={props.title}
          title={getText(event.title)}
          date={getText(event.date)}
          imageSrc={imageSrc}
          imageAlt={imageAlt}
          location={location}
          link={link}
          gaClickEvent={props.gaClickEvent}
          gaActionText={props.gaActionText}
        />
      );
    })
  );

  const eventProps = props.events;
  // Render items if data exists
  const events = (eventProps && eventProps.length) ?
    renderEvents(eventProps) : <div>There are no events under this section.</div>;

  return (
    <div
      id={`tabpanel-${props.num}`}
      role="tabpanel"
      aria-hidden={props.ariaHidden}
      aria-labelledby={`tab-${props.num}`}
    >
      <ul
        className={`${props.className}`}
      >
        <li>
          <h3 tabIndex="0">{props.title}</h3>
          <ul>
            {events}
          </ul>
        </li>
      </ul>
    </div>
  );
};

SectionItem.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  lang: PropTypes.string,
  title: PropTypes.string,
  events: PropTypes.array,
  ariaHidden: PropTypes.string,
  num: PropTypes.number,
  gaActionText: PropTypes.string,
  gaClickEvent: PropTypes.func,
};

SectionItem.defaultProps = {
  id: 'sectionItem',
  className: 'sectionItem',
  ariaHidden: 'false',
  lang: 'en',
  title: '',
  events: [],
};

export default SectionItem;
