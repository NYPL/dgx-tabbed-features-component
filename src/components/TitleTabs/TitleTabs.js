// Import React libraries
import React from 'react';
import PropTypes from 'prop-types';
// Import components
import Tab from './../Tab/Tab.js';
// Import miscellaneous libraries
import { map as _map } from 'underscore';

const TitleTabs = (props) => {
  /**
   * getSectionTitle(nameObj)
   * @desc Get and return the name string from the name object with fallback checks.
   * @param (Object) nameObj
   * @return String
   */
  const getSectionTitle = (nameObj = {}) => (
    (nameObj[props.lang] && nameObj[props.lang].text) ?
      nameObj[props.lang].text : ''
  );

  /**
   * renderTitles(titles)
   * @desc Render Tab Components.
   * @param (Array) titles
   * @return Object
   */
  const renderTitles = (titles) => (
    _map(titles, (element, i) =>
      <Tab
        key={i}
        num={i}
        action={props.action}
        title={getSectionTitle(element.name)}
        index={props.index}
        gaActionText={props.gaActionText}
        gaClickEvent={props.gaClickEvent}
      />
    )
  );

  return (
    <ul className={props.className} role="tablist">
      {renderTitles(props.titles)}
    </ul>
  );
};

TitleTabs.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  lang: PropTypes.string,
  titles: PropTypes.array,
  index: PropTypes.number,
  action: PropTypes.func,
  gaActionText: PropTypes.string,
  gaClickEvent: PropTypes.func,
};

TitleTabs.defaultProps = {
  id: 'titleTabs',
  className: 'titleTabs',
  lang: 'en',
  titles: [],
  index: 0,
  action: () => {},
};

export default TitleTabs;
