// Import React
import React from 'react';
import PropTypes from 'prop-types';
// Import components
import Section from './../Section/Section.js';
import TitleTabs from './../TitleTabs/TitleTabs.js';
// Utils
import { map as _map } from 'underscore';

const TabbedComponent = (props) => {
  /**
   * _getSectionTitle(nameObj)
   * @desc Get and return the name string from the name object with fallback checks.
   * @param (Object) nameObj
   * @return String
   */
  const getSectionTitle = (nameObj = {}) => (
    (nameObj[props.lang] && nameObj[props.lang].text) ?
      nameObj[props.lang].text : ''
  );

  /**
   * renderSections(sections, index)
   * @desc Render Section Components.
   * @param (Array) sections
   * @param (Index) num
   * @return Object
   */
  const renderSections = (sections, index) => (
    _map(sections, (element, i) => {
      const title = getSectionTitle(element.name);
      const link = (element.link) ? element.link : '';
      const idTitle = title.replace(/\s/g, '');
      const activeTab = i === index;
      const active = (activeTab) ? 'active' : '';
      let ariaHidden = 'false';

      // Each section can display up to four events.
      let events = element.slots;

      // But if the section that is being rendered is not active, only render the first event
      // which is used on mobile.
      if (!activeTab) {
        events = element.slots.slice(0, 1);
        ariaHidden = 'true';
      }

      return (
        <Section
          key={i}
          num={i}
          id={`Section-${idTitle}`}
          title={title}
          events={events}
          link={link}
          ariaHidden={ariaHidden}
          className={`${active} ${props.className}-section`}
          gaClickEvent={props.gaClickEvent}
          gaActionText={props.gaActionText}
        />
      );
    })
  );

  const happeningData = props.items;
  const index = props.index;

  if (props.error || !happeningData || !happeningData.length) {
    // If the items were fetched and parsed but are empty
    // then display an error message.
    return (
      <div className="errorMessage">
        We're sorry. Information isn't available for this feature.
      </div>
    );
  }

  return (
    <div className={props.className} id={props.id}>
      <div className={`${props.className}-titleWrapper`}>
        <TitleTabs
          titles={happeningData}
          index={index}
          action={props.action}
          gaClickEvent={props.gaClickEvent}
          gaActionText={props.gaActionText}
        />
      </div>
      {renderSections(happeningData, index)}
    </div>
  );
};

TabbedComponent.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  lang: PropTypes.string,
  items: PropTypes.array,
  index: PropTypes.number,
  action: PropTypes.func,
  error: PropTypes.string,
  gaClickEvent: PropTypes.func,
  gaActionText: PropTypes.string,
};

TabbedComponent.defaultProps = {
  id: 'tabbedComponent',
  className: 'tabbedComponent',
  lang: 'en',
  items: [],
  index: 0,
  action: () => {},
};

export default TabbedComponent;
