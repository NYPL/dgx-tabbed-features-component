// Import React libraries
import React from 'react';
import PropTypes from 'prop-types';

const Tab = (props) => {
  const active = (props.num === props.index) ? 'active' : '';
  const onKeyDown = (event) => {
    if (event.keyCode === 32 || event.keyCode === 13) {
      // Fire off GA click event tracking
      if (props.gaClickEvent && props.gaActionText) {
        props.gaClickEvent(`${props.gaActionText} Tab`, props.title);
      }
      props.action(props.num);
    }
  };
  const onClick = () => {
    // Fire off GA click event tracking
    if (props.gaClickEvent && props.gaActionText) {
      props.gaClickEvent(`${props.gaActionText} Tab`, props.title);
    }
    props.action(props.num);
  };

  return (
    <li
      className={active}
      id={`tab-${props.num}`}
      onClick={onClick}
      onKeyDown={onKeyDown}
      tabIndex="0"
      aria-controls={`tabpanel-${props.num}`}
      aria-selected={!!active}
      role="tab"
    >
      {props.title}
    </li>
  );
};

Tab.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  title: PropTypes.string,
  num: PropTypes.number,
  index: PropTypes.number,
  action: PropTypes.func,
  gaClickEvent: PropTypes.func,
  gaActionText: PropTypes.string,
};

Tab.defaultProps = {
  id: 'tab',
  className: '',
  title: '',
  num: 0,
  index: 0,
  action: () => {},
};

export default Tab;
