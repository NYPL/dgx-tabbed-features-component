// Import React
import React from 'react';
import PropTypes from 'prop-types';

const EventItem = ({
  title,
  headingTitle,
  date,
  className,
  imageSrc,
  imageAlt,
  link,
  location,
  gaClickEvent,
  gaActionText,
  num,
}) => {
  /**
   * getGaActionText()
   * Returns the string representation of the desired Google Analytics Action. Verifies if various
   * properties are defined to concatenate into a final string. The headingTitle property is
   * optional and represents the Tab heading title. The num property is optional and if
   * valid, it will be concatenated at the end.
   */
  const getGaActionText = () => {
    let actionText = gaActionText;

    if (headingTitle) {
      actionText = `${actionText} - ${headingTitle}`;
    }

    if (num >= 0) {
      actionText = `${actionText} - ${num + 1}`;
    }

    return actionText;
  };

  return (
    <li className={className} tabIndex="0">
      <a
        className={`${className}-link`}
        href={link}
        onClick={gaClickEvent && gaActionText ?
          () => gaClickEvent(getGaActionText(), link) : null
        }
      >
        <img className={`${className}-image`} src={imageSrc} alt={imageAlt} />
        <span className={`${className}-title`}>
          {title}
        </span>
      </a>
      <p className={`${className}-date`}>
        {date}
      </p>
      <p className={`${className}-location`}>
        {location}
      </p>
    </li>
  );
};

EventItem.propTypes = {
  className: PropTypes.string,
  lang: PropTypes.string,
  date: PropTypes.string,
  imageSrc: PropTypes.string,
  imageAlt: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  headingTitle: PropTypes.string,
  location: PropTypes.string,
  gaActionText: PropTypes.string,
  gaClickEvent: PropTypes.func,
  num: PropTypes.number,
};

EventItem.defaultProps = {
  className: 'eventItem',
  lang: 'en',
};

export default EventItem;
